import React from 'react';
import "../styles/home.scss";
import Navbar from "./navbar";
function Home() {
    return (
        <div className="home">
            <Navbar/>
        </div>
    );
}

export default Home;