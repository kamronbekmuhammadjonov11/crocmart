
import './App.css';
import Home from "./components/home";
import {BrowserRouter , Route, Switch} from "react-router-dom";
import Admin from "./adminPanelComponents/admin";

function App() {
  return (
    <div className="App">
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Home}/>
                <Route path="/admin"  component={Admin}/>
            </Switch>
        </BrowserRouter>
    </div>
  );
}

export default App;
